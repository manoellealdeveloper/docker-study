# Docker

# O que é?

<p> Plataforma para desenvolvimento, provisionamento e execução de aplicações usando tecnologia de containers. </p>

# O que é container?

<p>Simplesmente um isolamento de recursos, porém, um containter não é uma máquina virtual.</p>

# Por que usar?

* Velocidade
  1. Nenhum sistema inteiro para iniciar;
  2. Aplicações iniciam mais rápido.
* Portabilidade
  1. Menos dependência entre as camadas do processo de desenvolvimento;
  2. Facilidade em mover para múltiplas arquiteturas.
* Eficiência
  1. Empacotamento de uma aplicação inteira em imagens;
  2. Agilidade e padronização na entrega dos serviços.

# Arquitetura Docker

* Docker Engine;
* Docker Client;
* Docker Object;
* Docker Registry.

![image](https://user-images.githubusercontent.com/20096045/140384014-6d5a81f1-365a-4e6e-98f7-40afa7d63583.png)


# Docker Engine (Motor)

<p> E o responsável pela comunicação entre a imagem, volumes, containers e network, é a ingrenagem que comunica todos os componentes docker. </p> 

# Docker Client (CLI)

<p> E o responsável pela entrada de dados do usuário, é a cli que envia os comando para a engine.</p>

# Docker objects

* Imagens (modelo): É um modelo ou template usado para criar um container.
* Containers (instância de uma imagem): e a execução de uma imagem de fato.
* Networking: Capacidade dos containers se comunicar entre si ou com outros host disponível.
* Storage: Onde os dados serão armazenados fora do container, já que, o container e volátil, ou seja, quando a execução de um container e encerrada, os dados contidos nessa execução são perdidos.

# Docker Registry

<p> É o repositório onde se armazena as imagens criadas. </p>

# Estrutura Docker image

<p> Normalmente uma imagem é composta por várias outras imagens, ficando em camadas, a primeira camada ou a base imagem normalmente contem a imagem do Sistema operacional, essas imagens vão se sobrepondo até ser possível criar o container necessário para rodar a aplicação. </p>

![image](https://user-images.githubusercontent.com/20096045/140384481-aa4b03e8-4e86-48a4-a197-0955cc0aa585.png)

# Principal repositório publico

https://hub.docker.com/

# Docker Lifecycle

<b>Elementos</b>

 * dockerfile
 * image
 * registry
 * container

# Docker Object: Network

<p>Capacidade dos containers de se comunicar entre si ou com hosts externos através de uma abstração de rede intermediado pelo network driver plugin disponibilizado pela Docker engine.</p>

<b>Network driver bridge – Comunicação entre host e container</b>

* Rede especial padrão;
* Faz ponte entre host e os containers;
* Cria camada que permite container ter sua própria eth;
* Aleatoreamente é criada uma subnet PRIVADA;
* DHCP interno.

<b>Port expose</b>

<p>mapeamento entre a porta do host com a porta do container.</p>

<b>Network driver host</b>

* Não há isolamento de rede entre host e container;
* Outros aspectos do container continuam isolados.

# Docker Object: Volumes

Nas camadas de imagem dentro do container tem somente permissão de leitura, tendo somente uma camada de escrita na camada mais superior do container,ou seja, fora do ciclo de vida do container, todos os dados escritos serão perdidos.

1. Bind Mount
2. Volume
3. tmpfs mount* (somente em sistemas linux)

# Bind Mount

* Simples e eficiênte;
* Mapeamento de dados entre o host hospedeiro e o container;
* não escalável;
* arquivo e diretório e criado sob demanda;
* arquivos origem(host) "sobrescrevem" dados destino(container).

# Docker Volume

* Volume entre o host hospedeiro e o container;
* Completamente gerenciado pelo docker;
* Uso em produção.

# TMPFS Mount

* Grava na memória ram do hospedeiro
* É mais rápido, porém, como a memória ram é voláti

# Comandos Docker 

* docker images: lista as imagens baixadas 
* docker rmi <nome_imagem>: remove a imagem
* docker image prune -a: remove todas as imagens sem uso 
* docker pull <nome_imagem>: comando para baixar uma imagem
		Ex: docker pull ubuntu:18.04
* docker run [options] <nome_imagem> ou <id_imagem>[command][args]: comando para executar uma imagem e criar um container
* docker run –rm -it <nome_imagem>: comando para executar o container entrar no terminal e encerrar quando sair do container
* docker run –name <alias> -itd <nome_imagem>: comando para executar o container em background (não vai parar a execução imediatamente) e informando um nome a escolha como alias
* docker ps -a: comando para listar containers
* docker ps: comando para executar containers em execução
* docker stop <nome_container>: para a execução de um container ativo
* docker start <nome_container>: inicia a execução de um container inativo
* docker pause <nome_container>: pausa a execução de um container ativo, a diferença do pause para o stop, é que no pause os recursos computacionais ainda ficam alocados para ele, ou seja ele congela o container
* docker unpause <nome_container>: ativa um container que estava em pausa
* docker rm <nome_container>: remove o container da lista de containers
* docker rm -f <nome_container>: força a remoção do container
* docker container prune: remove todos os containers inativos
* docker inspect <nome_container>: apresenta os dados do container no formato json
* docker logs <nome_container>: apresenta todo o log de execução do container em questão
* docker logs -f <nome_container>: vai printando o final do arquivo conforme a execução
* docker attach <nome_container>: comando para interagir com o container, ou seja, entrar no terminal shell do SO do container
* Ctrl + pq: Comando para sair do container sem inativa-lo
* docker exec <nome_container> <comando>: executa um comando no container sem entrar dentro do shell interativo dele 
* docker exec -it <nome_container> bash: entra no shell do container em questão
* docker cp <nome_container>:<path_arquivo_origem> <path_destino>
* docker cp <path_arquivo_origem> <nome_container>:<path_destino>
* docker export <nome_container> | gzip > <nome>.tar.gz
* zcat <nome_arquivo_compactado> | docker import - <nome_importacao>
* docker run -it –name <nome_container> <imagem> bash
* docker search <palavra_chave>: pesquisa uma imagem no docker hub atraves do cli
* docker stats: exibe o consumo dos containers em execução 
* docker info: exibe os dados da arquitetura docker na máquina
* docker version: exibe a versão instalada
* docker network ls: lista os drivers de rede instalados
* docker network inspect <nome_driver>: apresenta as informações do driver no formato json
* docker inspect <nome_container> | grep IP: retorna as informaçoes de ip do container
* docker run -d –name <nome_container> -p 8080:80  <nome_imagem>: executa uma imagem mapeando a porta 
* docker run -d –name <nome_container> -P <nome_imagem>: Execta uma imagem mapeando a porta de forma automática
* docker port <nome_container>: exibe a porta do container
* docker network inspect host: apresenta os dados de rede no modo host (driver host)
* docker run -itd –net host –name <nome> <nome_imagem>: executa imagem sem ponte de conexeção (driver host)
* ls /var/lib/docker: lista os repositórios da docker engine
* docker run -itd --name <nome> -p <porta_container>:<porta_host> -v <path_host>:<path:container> <imagem>: comando para executar uma imagem, passando dados de porta e path de onde será realizada a persistência de dados no formato Bind mount
	Ex: docker run -itd --name apache -p 9090:80 -v /home/manoel/Documents/site:/usr/local/apache2/htdocs httpd:2.4
* docker volume create <nome_volume>: cria um volume
* docker volume ls: lista os volumes criados
	
# Criações de containers uteis
	
1. Apache
	
	1. Executar o comando de execução da imagem:
	
	* Para mapeamento de porta manual:
		
	`sudo docker run -d --name apache_server -p 9090:80 httpd`
	
	![image](https://user-images.githubusercontent.com/20096045/140559255-81e63b83-91ba-4ec5-848f-7bd7e03d670c.png)
	
	* Para mapeamento de porta automático:
	
	`sudo docker run -d --name apache_server_auto -P httpd`
	
	![image](https://user-images.githubusercontent.com/20096045/140559434-41443fb7-3c89-4d46-9a03-2e5b629511f4.png)
	
	2. Verificar o mapeamento de porta do container:
	
	* utilizar o comando docker ps ou docker <container> port
	
	![image](https://user-images.githubusercontent.com/20096045/140559620-948139f7-b168-474b-89e1-389ecafceb22.png)

	3. Verificar se o serviço do apache está ativo: digitar no navegador ou no curl localhost:<porta>
	
	![image](https://user-images.githubusercontent.com/20096045/140559758-b81b67bc-f334-4dbc-a9b9-9ad4e08ea2ed.png)
	
	![image](https://user-images.githubusercontent.com/20096045/140559871-58b5cdf5-9af1-4c74-8284-84d3c05f6fad.png)



# Referências
	
* http://pointful.github.io/docker-intro/
* https://www.aquasec.com/wiki/display/containers/Docker+Containers+vs.+Virtual+Machines
* https://www.aquasec.com/wiki/display/containers/Docker+Architecture
* https://docs.docker.com/engine/docker-overview/
* https://www.docker.com/resources
* http://edu.delestra.com/docker-slides/
* https://docs.docker.com/engine/reference/commandline/docker/
* https://www.portainer.io/installation/
* https://hub.docker.com/_/mysql
* https://hub.docker.com/_/postgres/
* https://hub.docker.com/r/governmentpaas/psql
* https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file
* https://docs.docker.com/network/
* https://www.aquasec.com/wiki/display/containers/Docker+Networking+101
* https://success.docker.com/article/networking
* https://docs.docker.com/config/containers/container-networking/
* https://blog.docker.com/2016/12/understanding-docker-networking-drivers-use-cases/
* https://docs.docker.com/network/iptables/

